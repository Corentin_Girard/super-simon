Vue.component('score-counter', {
    props: ['highScore'],
    template: `
        <div id="highscore">
            <h2>Highscore : {{highScore}}</h2>
        </div>
    `
})
