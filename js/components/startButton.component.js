Vue.component('start-button', {
    template: `
        <div id="start-button-div">
            <button 
                id="start-button" 
                v-on:click="$emit('start-game')">
                Jouer
            </button>
        </div>
    `
});
