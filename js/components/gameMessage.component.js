Vue.component('game-message', {
    props: ['gameMessage'],
    template: `
        <div id="game-message">
            <h2>{{gameMessage}}</h2>
        </div>
    `
});
