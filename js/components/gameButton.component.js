Vue.component('game-button', {
    props: ['color'],
    template: `
        <div
            :id="color.label"
            :class="{highlighted: color.highlighted}"
            v-on:mousedown="$emit('push')"
            v-on:mouseup="$emit('release')">
        </div>`
})
