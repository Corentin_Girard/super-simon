Vue.component('level-info', {
    props: ['levelCount'],
    template: `
        <div id="level-info">
            <h1>Level {{levelCount}}</h1>
        </div>
    `
});
