const timer = ms => new Promise(res => setTimeout(res, ms));

let app = new Vue({
    el: '#simon',
    data: {
        gameIsInProgress: false,
        playerTurn: false,
        levelCount: 1,
        highScore: 0,
        gmSelectedColors: [], // Tableau contenant la combinaison de couleurs choisies au hasard
        playerNbSelectedColors: 0, // Nombre de couleurs jouées par le joueur
        displayedButtonTime: 400, // Temps d'affichage initial des bouton aléatoire (en ms)
        gameMessage: "",
        colors: [ // Tableau des couleurs
            {id: 1, label: 'rouge', audio:'red', highlighted: false},
            {id: 2, label: 'jaune', audio:'yellow', highlighted: false},
            {id: 3, label: 'bleu', audio:'blue', highlighted: false},
            {id: 4, label: 'vert', audio:'green', highlighted: false},
        ],
        paths: new Map()
    },
    methods: {
        /**
         * Initialisation du jeu
         */
        start() {
            this.gameIsInProgress = true;

            // Gestion des chemins des fichiers audio
            this.paths.set('salut', 'audio/salut.mp3');
            this.paths.set('letsGo', 'audio/ok-lets-go.mp3');
            this.paths.set('aie', 'audio/aie.mp3');
            this.paths.set('red', 'audio/red.mp3');
            this.paths.set('blue', 'audio/blue.mp3');
            this.paths.set('green', 'audio/green.mp3');
            this.paths.set('yellow', 'audio/yellow.mp3');

            this.roundLoop();
        },
        /**
         * Boucle de jeu pour un level
         * @returns {Promise<void>}
         */
        async roundLoop() {
            this.playerNbSelectedColors = 0;

            for (let index = 0; index < this.levelCount; index++) {
                let color = this.choseGmColor(index);

                color.highlighted = true;
                this.playAudio(this.paths.get(color.audio));

                await timer(this.displayedButtonTime);

                color.highlighted = false;

                await timer(300);
            }
            this.playerTurn = true;
            this.gameMessage = "À toi de jouer";
        },
        /**
         * Choix d'une couleur aléatoire
         * @param index
         * @returns {{highlighted: boolean, id: number, label: string, audio: string}|{highlighted: boolean, id: number, label: string, audio: string}|{highlighted: boolean, id: number, label: string, audio: string}|{highlighted: boolean, id: number, label: string, audio: string}|*}
         */
        choseGmColor(index) {
            if (index >= this.gmSelectedColors.length || this.gmSelectedColors.length === 0) {
                let n = Math.floor(Math.random() * 4) + 1;

                let color = this.colors.find((e) => {
                    return e.id === n;
                });

                this.gmSelectedColors.push(color);

                return color;
            } else {
                return this.gmSelectedColors[index];
            }
        },
        /**
         * Récupération de la couleur sélectionnée par le joueur
         * @param colorId
         * @returns {Promise<void>}
         */
        async push(colorId) {
            if (this.playerTurn) {
                let color = this.colors.find((e) => {
                    return e.id === colorId;
                });
                color.highlighted = true;

                if (color.id !== this.gmSelectedColors[this.playerNbSelectedColors].id) {
                    this.restart();
                } else {
                    this.playAudio(this.paths.get(color.audio));
                    this.playerNbSelectedColors++;
                }
            }

        },
        /**
         * Après le clique sur une couleur par le joueur
         * @param colorId
         * @returns {Promise<void>}
         */
        async release(colorId) {
            let color = this.colors.find((e) => {
                return e.id === colorId;
            });
            color.highlighted = false;

            if (this.playerNbSelectedColors === this.gmSelectedColors.length) {
                this.gameMessage = "Bravo !";
                this.playerTurn = false;
                await timer(1000);
                this.nextRound();
            }
        },
        /**
         * Passage au niveau suivant
         * @returns {Promise<void>}
         */
        async nextRound() {
            this.levelCount++;
            if (this.levelCount > this.highScore){
                this.highScore++;
            }

            if (this.levelCount % 5 === 0) {
                this.playAudio(this.paths.get('letsGo'));
                this.gameMessage = "Zzzzzé partiii on accèlèrHAN !!!!!";

                this.displayedButtonTime = this.displayedButtonTime * 0.6;

                await timer(2000);
            }

            this.gameMessage = "";
            this.roundLoop();
        },
        /**
         * Reset du jeu après un mauvais choix
         * @returns {Promise<void>}
         */
        async restart() {
            this.playAudio(this.paths.get('aie'));
            this.gameMessage = "Eh non c'est perdu.... Il fallait cliquer sur " + this.gmSelectedColors[this.playerNbSelectedColors].label;
            this.playerTurn = false;

            await timer(3000);

            this.playerTurn = false;
            this.levelCount = 1;
            this.gmSelectedColors = [];
            this.gameMessage = "";
            this.displayedButtonTime = 800;
            this.playerNbSelectedColors = 0;
            this.roundLoop();
        },
        /**
         * Gestion de l'audio
         * @param path
         */
        playAudio(path) {
            let audio = new Audio(path);
            audio.play();
        }
    },
    mounted: function () {
        this.$nextTick(function () {
            let audio = new Audio('audio/salut.mp3');
            audio.play();
        })
    }
});
